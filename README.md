This docker image aims at making docker based Nextcloud backups safe and easily doable with automation.

---

## Prerequisites

This is not a complete image. For this to work you'll need the following in place:

1. MySQL or MariaDB database backend.
2. Sendgrid SMTP server (for email notifications).

# How to use this image

This image is designed to work without any intervention to your current Nextcloud install. Therefore you can use this with your current setup, without any amount of down time. If you don't want to automate the process, you can run it one off as well using `docker container create ...`.

## Environment variables

You'll need a couple of environment variables to be set upfor this image to work.

- `NEXTCLOUD_HOST` Name of the running Nextcloud container.
- `DATABASE_HOST` Name of the MariaDB/MySQL database container.
- `EMOJI_SUPPORT` If you have emojis enabled, set this to 1.
- `SMTP_PASSWORD` Your Sendgrid API key.
- `SMTP_FROM_ADDRESS` Same as `MAIL_FROM_ADDRESS` of Nextcloud.
- `DOCKER_HOST` This is totally optional. By default you'll need to mount your local docker socket to `/run/docker` inside the container. If you wish to change that destination, set this variable to the respective location.

> Currently this image doesn't support remote docker engine endpoints.

## Volumes and Bind Mounts

1. **Nextcloud's data volume**

    This image will create an archive out of Nextcloud's data directory. Share that volume by mounting it at `/source` inside this container. If you went with Nextcloud's default selection, this volume is the one mounted at `/var/www/html` in Nextcloud. Make sure you mount it as Read Only like below

    ```
    docker run ... -v nextcloud-data:/source:ro ... imdebdut/nextcloud-backup-companion:latest
    ```

2. **Save the backups**

    The backups are saved inside `/backups` directory. Each backup is kept inside a subdirectory named against current date and time. Save the data using a volume or bind mount of your preference.

3. **Bind mount docker socket**

    Bind the docker socket at `/run/docker.sock`.

4. **Crontabs**

    This image uses cron to schedule the automatic backups. Bind mount any of the directories inside `/jobs` to the host to add/remove jobs anytime. Cron will automatically restart afterwards.

## Networks

For this container to be able to communicate with the others, share the same custom network with Nextcloud and its database containers.

# backup utility

This container comes with a Python utility named `backup`. This comes with a couple of options explained below.

- `--notify` This option is used to send a notification email to the server's admin. It takes two more string arguments, used to clarify the time left before the backup process will begin.
    ```
    backup --notify 24 hours
    ```
- `-d` This tells `backup` to not log to stdout.
- `--db` Only backup the database.
- `--data` Only backup the Nextcloud data.
- `--all` Back up everything.

# One off usage

Consider the following:
- "nextcloud" is Nextcloud conatiner's name.
- "db" is the database container's name.
- "data" is Nextcloud's persistent data volume.

To do a quick backup of everything, something like the following will suffice

```
mkdir backups
docker run --rm --name backup \
    -e NEXTCLOUD_HOST=nextcloud \
    -e DATABASE_HOST=db \
    -e SMTP_PASSWORD=$SMTP_PASSWORD \
    -e SMTP_FROM_ADDRESS=noreply@domain.com \
    -v /run/docker.sock:/run/docker.sock:ro \
    -v data:/source:ro \
    -v $PWD/backups:/backups \
    --network net imdebdut/nextcloud-backup-companion:latest backup --all
```

# Automatic backup setup

For my example Nextcloud install, refer to [this compose file](https://bitbucket.org/debdut-images/backup-companion/src/master/nextcloud-compose.yml). Add a backup service definition at the end.

```
    backup:
        image: "imdebdut/nextcloud-backup-companion:latest"
        container_name: "backup-companion"
        environment:
            - NEXTCLOUD_HOST=nextcloud
            - DATABASE_HOST=db
            - SMTP_PASSWORD=${SMTP_PASSWORD}
            - SMTP_FROM_ADDRESS=noreply@domain.com
        volumes:
            - "/run/docker.sock:/run/docker.sock:ro"
            - "data:/source:ro"
            - "./backups:/backups"
            - "./cronjobs:/jobs/always"
        networks: ["net"]
```

Afterwards, run `docker-compose up -d backup` to start the container.

## Adding cronjobs

If you navigate into the bind mounted `cronjobs` directory, you'll find a number of directories:

```
15min
always
daily
hourly
monthly
weekly
```

If you want to have daily backups of your database, write a script with `backup -d --db` as its content, and put it in the `daily` directory. And that's it. If you want a more granular control, like "Backup every Monday morning 9AM, and send a 24 hours prior notice", put a file in `always` with the following contents

```
0 9 * * SUN backup -d --notify 24 hours
0 9 * * MON backup -d --all
```

> The always directory accepts crontab files instead of scripts/programs.

# Restoring the backups

Restoring the backup is quite an easy process. Edit your `docker-compose` file and mount the directory/volume that stored your backups. Let's say you mounted it at `/backups`. First start up the database container.

```
docker-compose up -d <database service name>
```

We first need to let the mariadb container create the user and the database. Once it's done, execute the following

```
docker-compose exec <database service name> bash -c 'mysql -u$MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE < /backups/<subdirectory>/database.sql'
```

The backups are placed in a folder, which is named in a `%d-%m-%Y_%H:%M:%S` format. Make sure you use the correct backup while restoring.

Next we need to restore all of Nextcloud's files. For this, execute the following command

```
docker-compose run <nextcloud service name> bash -c 'tar xpvf /backups/<subdirectory>/data.tar.gz -C /var/www/html'
```

Finally run `docker-compose up -d`. After giving it a few seconds, head over to the domain/subdomain that's going to host your restored Nextcloud instance.
