from imdebdut/cron:alpine
run apk add --no-cache python3 py3-pip && \
    pip3 install --no-cache-dir pip --upgrade && \
    pip3 install --no-cache-dir docker sendgrid

volume ["/source", "/backups"]
env DOCKER_HOST="/run/docker.sock"

copy --chown=0:0 backup /app/
run chmod +x /app/backup && \
    touch /var/log/backup.log
workdir "/"
entrypoint ["entrypoint.sh"]
cmd ["logfile", "/var/log/backup.log"]
